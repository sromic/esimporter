import java.nio.file.Paths

import akka.stream.IOResult
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink}
import akka.util.ByteString

import scala.concurrent.Future

/**
 * Created by simun on 27.9.2016..
 */
package object run {

  def sumElementsSink[T] = Sink.fold[Int, T](0) { (sum, _) =>
    val newSum = sum + 1

    if (newSum % 5000 == 0) {
      print(s"\rCount: $newSum")
    }

    newSum
  }

  def write2File[T](filename: String): Sink[T, Future[IOResult]] = {
    Flow[T]
      .map(s => ByteString(s + "\n"))
      .toMat(FileIO.toPath(Paths.get(filename)))(Keep.right)
  }
}
