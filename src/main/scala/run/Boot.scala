package run

import java.io.FileInputStream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.sksamuel.elastic4s.BulkCompatibleDefinition
import com.sksamuel.elastic4s.streams.RequestBuilder
import org.apache.commons.io.input.BOMInputStream
import org.json4s.Xml.toJson
import org.json4s.jackson.JsonMethods._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.io.Source
import scala.xml.XML
import scala.xml.pull.{EvElemEnd, EvElemStart, EvText, XMLEventReader}


/**
 * Created by simun on 26.9.2016..
 */
object Boot extends App {

  implicit lazy val system = ActorSystem("ESFeeder")
  implicit lazy val materializer = ActorMaterializer()
  implicit lazy val ec = system.dispatcher


  val xml = new XMLEventReader(
    Source.fromInputStream(
      new BOMInputStream(new FileInputStream("StandardBMEcat_2015-10-18.xml"))))

  var buf = ArrayBuffer[String]()

  var transfromProduct = false

  var productList = List[String]()

  //if 100 product collected clean buffer and bulk insert to ES
  def transformToJson(buf: ArrayBuffer[String]) = {
    val s = buf.mkString
    val x = XML.loadString(s)

    productList = productList :+ compact(render(toJson(x)))

    if(productList.size == 1000) {
      writeToElasticsearch(productList)
      productList = List[String]()
    }
  }

  import com.sksamuel.elastic4s.ElasticDsl._
  import database.ElasticSearch._

  def writeToElasticsearch(bulkList: List[String]): Unit = {

    val ops2: List[BulkCompatibleDefinition] = for (product <- bulkList) yield { builder("products").request(product) }

    val result = Await.result(esClient.execute(bulk(ops2: _*)), Duration.Inf)

    println(result.hasSuccesses+ " " + result.hasFailures)
  }

  def parse(xml: XMLEventReader): Unit = {
    def loop(currNode: List[String]): Unit = {
      if (xml.hasNext) {
        xml.next match {
          case EvElemStart(_, "PRODUCT", _, _) =>
            //println("###########product start###########")
            transfromProduct = true
            buf += "<PRODUCT>"
            loop("PRODUCT" :: currNode)
          case EvElemStart(_, label, _, _) =>
            //println("Start element: " + label)
            if(transfromProduct)
              buf += "<" + label + ">"
            loop(label :: currNode)
          case EvElemEnd(_, "PRODUCT") =>
            //println("###########product end###########")
            buf += "</PRODUCT>"

            transformToJson(buf)

            buf.clear()
            transfromProduct = false
            loop(currNode.tail)
          case EvElemEnd(_, label) =>
            //println("End element: " + label)
            if (transfromProduct)
              buf += "</" + label + ">"
            loop(currNode.tail)
          case EvText(text) =>
           //println(text)
            if(transfromProduct)
              buf += (text)
            loop(currNode)
          case _ => loop(currNode)
        }
      }
    }
    loop(List.empty)
  }

  parse(xml)

  def builder(indexName: String) = new RequestBuilder[String] {
    def request(e: String): BulkCompatibleDefinition = {
      index into indexName -> "products" source e
    }
  }

  //Await.result(bulkInsertToES, Duration.Inf)

  Await.result(system.terminate(), Duration.Inf)
}