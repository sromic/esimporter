package run

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.sksamuel.elastic4s.RichSearchHit

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
 * Created by simun on 26.9.2016..
 */
object BootES extends App {

  import com.sksamuel.elastic4s.ElasticDsl._
  import com.sksamuel.elastic4s.streams.ReactiveElastic._
  import database.ElasticSearch._

  implicit lazy val duration = Duration.Inf
  implicit lazy val actorSystem = ActorSystem("ESStream")
  implicit lazy val materializer = ActorMaterializer()
  implicit lazy val ec = actorSystem.dispatcher

  val publisherSource = Source.fromPublisher(esClient.publisher("products", keepAlive = "1m"))
  val publisherSourceSearch = Source.fromPublisher(esClient.publisher(search in "products" query "1" scroll "10m"))

  val flow: Flow[RichSearchHit, String, NotUsed] = Flow[RichSearchHit].map[String] { richSeachHit =>
    richSeachHit.sourceAsString
  }

  val sink = Sink foreach println

  val resultFromSink = publisherSource via flow runWith write2File[String]("products-31k.json")

  Await.result(resultFromSink, Duration.Inf)

  Await.result(actorSystem.terminate(), Duration.Inf)

}