lazy val `reactive-streams` = project
  .in(file("."))
  .enablePlugins(JavaAppPackaging)

name := "ESFeeder"

version := "1.0"

scalaVersion := "2.11.8"

//resolvers
resolvers += Resolver.jcenterRepo

// add conf/ folder
unmanagedResourceDirectories in Compile += baseDirectory.value / "conf"


//mainClass in (Compile, run) := Some("run.BootES")

daemonUser.in(Docker) := "root"
maintainer.in(Docker) := "Simun Romic"
version.in(Docker)    := "latest"
dockerBaseImage       := "java:8"
dockerExposedPorts    := Vector(2552, 8080)
dockerRepository      := Some("sromic")

libraryDependencies ++= Seq(
  "com.iheart" %% "ficus" % "1.2.3",
  "com.sksamuel.elastic4s" %% "elastic4s-core" % "2.4.0",
  "com.sksamuel.elastic4s" %% "elastic4s-streams" % "2.4.0",
  "com.typesafe.play" %% "play-json" % "2.5.8",
  "com.typesafe.akka" %% "akka-stream" % "2.4.10",
  "org.json4s" %% "json4s-native" % "3.5.0",
  "org.json4s" %% "json4s-jackson" % "3.5.0",
  "commons-io" % "commons-io" % "2.3"
)